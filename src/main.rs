use std::env;
use std::path;

use ggez;
use ggez::{Context, GameResult};
use ggez::conf::{FullscreenType, WindowSetup};
use ggez::conf::WindowMode;
use ggez::event;
use ggez::graphics;
use ggez::nalgebra as na;
use ggez::event::{KeyCode, KeyMods};

struct Assets {
    background: graphics::Image,
    light_fury: graphics::Image,
}

impl Assets {
    fn new(ctx: &mut Context) -> GameResult<Assets> {
        let background_image = graphics::Image::new(ctx, "/ocean_level.jpg")?;
        let light_fury_image = graphics::Image::new(ctx, "/dragon.png")?;

        Ok(Assets{
            background: background_image,
            light_fury: light_fury_image,
        })
    }
}

struct MainState {
    pos_x: f32,
    pos_y: f32,
    assets: Assets,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let assets = Assets::new(ctx)?;
        let s = MainState{pos_x: 0.0, pos_y: 0.0, assets};
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult {
        Ok(())
    }

    fn draw(&mut self, _ctx: &mut Context) -> GameResult {
        graphics::clear(_ctx, [0.1, 0.2, 0.3, 1.0].into());

        let dp = graphics::DrawParam::new()
            .dest(na::Point2::new(graphics::drawable_size(_ctx).0, graphics::drawable_size(_ctx).1));
        let (width, height) = graphics::drawable_size(_ctx);
        graphics::draw(_ctx, &self.assets.background, (na::Point2::new(0.0, 0.0),))?;
        graphics::draw(_ctx, &self.assets.light_fury, (na::Point2::new(self.pos_x, self.pos_y),))?;

        graphics::present(_ctx)?;

        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymods: KeyMods,
        _repeat: bool,
    ) {
        let constant = 10.0;
        match keycode {
            KeyCode::Up => {
                self.pos_y -= constant;
            }
            KeyCode::Down => {
                self.pos_y += constant;
            }
            KeyCode::Left => {
                self.pos_x -= constant;
            }
            KeyCode::Right => {
                self.pos_x += constant;
            }
            _ => (),
        }
    }
}

fn main() -> GameResult {
    let resources_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else { path::PathBuf::from("./resources")};

    let window = WindowMode {
        width: 1090.0,
        height: 490.0,
        borderless: false,
        fullscreen_type: FullscreenType::Windowed,
        min_width: 0.0,
        max_width: 0.0,
        min_height: 0.0,
        max_height: 0.0,
        maximized: false,
        resizable: false,
    };

    let cb = ggez::ContextBuilder::new("super_simple", "Brandon")
        .add_resource_path(resources_dir)
        .window_mode(window);
    let (ctx, event_loop) = &mut cb.build()?;
    let state = &mut MainState::new(ctx)?;
    event::run(ctx, event_loop, state)
}
